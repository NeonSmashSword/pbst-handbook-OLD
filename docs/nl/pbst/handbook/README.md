
# PBST Handboek
## Uniform
De officiële PBST uniformen zijn te vinden in de winkel [hier](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

Als je geen Robux hebt, zijn de meeste faciliteiten uitgerust met uniforme gevers om je op weg te helpen. Wij raden u nog steeds aan om voor uw gemak een uniform te kopen.

Je moet een officieel PBST uniform dragen wanneer je een Pinewood faciliteit controleert.

**Locaties van uniforme givers in de faciliteiten**:
* [Pinewood Computer Core:](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) From the main spawn through the lobby, up the elevator (follow ‘Security Sector + Cafe)’, in the PBST room to the right
* [Pinewood Research Facility:](https://www.roblox.com/games/7692456/Pinewood-Research-Facility) From the main spawn into the room marked ‘PB SEC’, to the hallway between the baton giver and the coffee machine, changing room on the left
* [Pinewood Builders HQ:](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ) 4th floor, from elevators to the glass doors on the left, blue doors on the left
* [PBST Training Facility:](https://www.roblox.com/games/298521066/PBST-Training-Facility) At the right side of the spawn building
* [PBST Activity Center:](https://www.roblox.com/games/1564828419/PBST-Activity-Center) Into the main hallway of the lobby

Het dragen van een PET-uniform is niet toegestaan, de enige uitzondering is bij PBCC wanneer de kerntemperatuur het onmogelijk maakt om een standaard kern pak te dragen en een PET Hazmat pak is de enige manier om de kern in te voeren (3000C+). Zodra de kern tempratuur weer een standaard kern pak mogelijk maakt (3000C-), moet je terugschakelen naar dat pak.

## Dienstplicht
Je moet een officieel PBST uniform dragen om te kunnen voldoen aan je dienstplicht. Uw rang lebel moet ook aan zijn en ingesteld worden op Beveiliging (wat de standaard is), Hoewel je niet over de rank hoeft te beschikken maak je geen dienstplicht.

Als je ingesteld bent op **TMS** of **PET**, voer het commando `!setgroup PBST` uit

Als PBST-dienstfunctionaris zult u de faciliteit beschermen tegen elke dreiging die zich voordoet. Als je moet evacueren, probeer dan de anderen het hardst te helpen.

Om vrijuit te gaan, moet je de veiligheidskamer bezoeken en je uniform en PBST-wapens verwijderen, om de wapens te veranderen klik dan op het vak “remove tools.” Reset karakter indien nodig. Het wordt sterk aanbevolen dat u ook uw beveiligingsrank tag verwijdert door gebruik te maken van de opdracht`! Setgroup [PBST/TMS/PET/Group Name]`" om over te schakelen naar een neutrale groep zoals PET, PB of andere Pinewood groep waarin u zich bevindt, of gebruik "`! anktag uit`" commando om de rang tag volledig uit te schakelen.

## Pinewood Computer Kern (PBCC)
De [Pinewood Computer Kern](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) is de belangrijkste plek om te patrouilleren. Uw belangrijkste doel is om te voorkomen dat de kern smelt of bevriezen. De volgende grafiek laat je zien hoe verschillende instellingen de temperatuur beïnvloeden. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="Demo" width="1228" />

PBCC heeft ook verschillende ‘rampen’, waarvan sommige PBST vereisen om de bezoekers te evacueren. In het geval van een plasmatoeslag, aardbeving of overstroming door straling brengt men de mensen in gevaar.

**Extra info**: To calculate the TPS (temperture per second) I've added a calculator: <Calculator />

## Gebruik van wapens
Every PBST member has access to the standard baton, and Tiers receive additional weapons from the blue loadouts. Be sure to have your uniform on when taking these weapons, for you are considered off-duty without uniform and you may not use PBST Weapons off-duty. **PBST weapons may never be used to cause a melt-or freezedown.**

You have to give **two** warnings to people before you can kill them. If a visitor has been killed after three warnings and still returns, you can kill that person *without warning*. On-duty PBST members also have to follow these rules when using non-PBST weapons like the OP Weapons gamepass, or the pistol acquired from PBCC Credits or randomly spawned

In case of emergency, a Tier 4 or higher may grant you permission to restrict a room to **PBST only**. In that case, you can kill anyone who tries to enter and only let *on-duty PBST* in.

When off-duty, you may only use non-PBST Weapons, though you have more freedom with your usage. You can, for example, restrict rooms without needing permission, though excessive room restriction may count as "mass random killing".

Mass random killing and spawn killing are always forbidden, use the `!call` command to call PIA to your assistance if someone is mass random killing or spawn killing.

## De Mayhem Syndicaat
The Mayhem Syndicate (**TMS**) is the opposite of PBST. Where we intend to protect the core, they intend to melt or freeze it. You can recognize them by the red rank tag. They often join a game in a raid, and when this happens you are advised to call for backup.

The only place where TMS may not be killed (aside from spawn) is at the TMS loadouts near the cargo trains. You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. Do not camp at the TMS loadouts.

As with all armed hostiles, you are allowed to fight back if TMS attacks you, even if they seem to be on their way to get their loadouts.

If you are off-duty when TMS starts to raid and you want to participate, you have to pick a side and fight on that side until the raid ends. **Do not change sides mid-raid.** This also applies if you are neutral or off-duty and decide to participate anyway. Beware that choosing to the side for either TMS or PBST in a raid might get you set on KoS (Kill on Sight) by the other side.

## Dood op licht (KoS)
Any member of TMS is to be killed on sight.

Mutants are KoS as well. **PBST members may not become mutants while on duty.**

On-duty PBST may not put anyone else on KoS unless permission has been granted by a Tier 4+.

## Training
::: warning NOTE: Instructions in training are given in English, so understanding English is required. :::

Tier 4+ can host a training where you can earn [points]((../ranks-and-ranking-up/)). The schedule of these can be found at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility).

**Training Rules**:
* Luister naar de host en volg zijn/haar bestellingen.
* Neem niet uw ladingen af, de host zal u geven wat u nodig hebt in elke activiteit.
* Altijd in de training een PBST uniform dragen.
* Trainers kunnen toestemming om te spreken activeren (PTS), wat betekent dat chatten je gestraft zal krijgen. Chat ‘`;pts request`’ voor toestemming indien dit ingeschakeld is.
* Sommige zijn gemarkeerd als Disciplinaire Training, die strikter is en meer gericht is op discipline. Je wordt uit de training gezet als je je slecht gedraaid hebt.
* Eenmaal per week wordt er hooguit een Hardcore Training georganiseerd. In deze opleiding zal u zelfs de geringste fout maken. Als je tot het einde 'overleven' bent, kun je tot 20 punten verdienen.

More information about points can be found [here](../ranks-and-ranking-up/)

## Rang
You will receive the next Tier rank once you reach the required amount of points. With your new rank comes a new loadout in Pinewood facilities: Tier 1 gives you a more powerful baton, a riot shield, a taser, and a PBST pistol. With Tier 2 you receive all the former plus a rifle. At Tier 3 you receive all the former plus a submachine gun. These weapons can be found in the Security room at Pinewood facilities, and Cadets are not allowed in there.

::: tip Recent change! As of `02/28/2020`. Tier **1** evaluations are now a thing. It you want to go from `Cadet` -> `Tier 1`. You need to complete an evaluation.

You can read more about it [here](../ranks-and-ranking-up/#tier-evaluations). :::

Once you are promoted to Tier, you are expected to be a role model for Cadets. Any violations of the rules may result in a larger punishment. Especially Tier 3’s are expected to be the perfect representation of PBST at its best. **You have been warned.**

Tiers have access to the `!call PBST` command, which they can use for an imminent melt- or freezedown or a raider. This is to be used wisely.

As a Tier, you may be selected to assist at training, where you will be given temporary admin powers which are strictly for that specific training. Abuse of these powers will result in severe punishment.

## Niveau 4 (aka Special Defense, SD)
Tier 3’s who reach 800 points are eligible for an SD evaluation to become Tier 4. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “Passed SD Eval” until the Trainers choose to promote you to Tier 4. Having Discord is required.

Like Tier 4, you receive the ability to place a KoS in Pinewood facilities, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used responsibly.

Tier 4’s can host training with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Tier 4’s may not request training more than 3 days ahead of time. There also has to be a 2-hour gap at least between the end of one training and the start of another.

## Trainers
To become a Trainer, all the current Trainers have to vote on your promotion. Only Tier 4’s are eligible for this promotion.

Trainers can host trainings without restrictions, though the 2-hour gap rule still applies. Trainers are also responsible for some of PBST’s more administrative tasks, like points logging and promotions. Do not ask Trainers to log your points or promote you, be patient.

Trainers who are patrolling Pinewood facilities are exempt from most rules in the handbook, though the following reminders also apply to them.

# Herinneringen aan alle PBST leden
  * Houdt u zich te allen tijde aan het [ROBLOX Community Rules](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules).
  * Wees respectvol met je medespelers, toon niet alsof je de baas van het spel bent.
  * Volg de bestellingen van iedereen die je huilt.

::: warning NOTE: These rules can be changed at any time. So please check every now and then for a update on the handbook. :::
