---
home: true
heroImage: /PBST-Logo.png
actionText: Let's do this →
actionLink: pbst/
---

**_NOTE:_**
This version of the handbook is now PBST official, as stated by Csdi (the HoS) on the 9th of March 2020 [via this link](https://discordapp.com/channels/438134543837560832/459764670782504961/686693529577062457):

>A new website is now in use as the official PBST handbook! #pbst-handbook
Keep in mind the devforum handbook is official as well, so nothing has changed. 

-CSDI, HoS, 9th March 2020.

The official and original PBST devforum handbook can be found [here](https://devforum.roblox.com/t/pinewood-builders-security-team-handbook-11-10-2019/385368). Please support the official version as well :smile:.

 **DISCLAIMER!** Please ensure you read through EVERY rule carefully. Any rule changes will be made clear to everyone. Each rule has been catergorized into each section that suits the rule the most. Some rules may apply more to specific PBST roles than others. Punishments for breaking a rule depends on what rules has been broken, they can range from a simple warning to a Trello blacklist from PBST. If you encounter any rule breaking, contact an availiable high ranking member as soon as possible! 

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a></center>
