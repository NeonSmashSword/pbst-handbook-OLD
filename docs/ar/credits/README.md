---
sidebar: auto
---

# Credits where they're due
We would like to following people for helping us with the handbook.

## Backend + Development
* Stefano / superstefano4

## Translations
This section has been **TEMPORARILY** removed. It will return as soon as the first translation version reaches 100% completion.[If you would like to help, please click on this sentence.](https://translate.pinewood-builders.ga) website and submit a language to translate. Contact Super using [this email](mailto:stefano@stefanocoding.me), to make a request to add (or help create) a new language version of the handbook.


## Proofreading
### HoS (Head of Security)
* Csd | Csdi

### PIA (Pinewood Intelligence Agency)
* Omni | TheGreatOmni
### Trainers
* TenX
* AnuCat
* CombatSwift
* RogueVader1996
