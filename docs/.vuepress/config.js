module.exports = ctx => ({
    head: [
        ['link', {
            rel: 'icon',
            href: '/PBST-Logo.png'
        }],
        ['link', {
            rel: 'manifest',
            href: '/manifest.json'
        }],
        ['meta', {
            name: 'theme-color',
            content: '#3eaf7c'
        }]
    ],
    dest: 'public/',
    locales: {
        '/': {
            lang: 'en-US',
            title: 'PBST Handbook',
            description: 'The official PBST Handbook'
        },
        '/de/': {
            lang: 'de-DE',
            title: 'PBST Handbuch',
            description: 'Das offizielle PBST Handbuch'
        }
    },
    themeConfig: {
        repo: 'https://gitlab.com/TCOOfficiall/PBST-Handbook',
        editLinks: true,
        docsDir: 'docs/',
        logo: '/PBST-Logo.png',
        smoothScroll: true,
        yuu: {
            defaultDarkTheme: true,
        },

        algolia: ctx.isProd ? ({
            apiKey: '23e68311fa2c8ccb3b1b444c3dc76035',
            indexName: 'prod_pbst'
        }) : null,
        locales: {
            '/': {
                label: 'English (English)',
                selectText: 'Languages',
                ariaLabel: 'Select language',
                editLinkText: 'Edit this page on GitLab',
                lastUpdated: 'Last Updated',
                nav: require('./nav/en'),
                sidebar: {
                    '/pbst/': [
                        'handbook/',
                        'ranks-and-ranking-up/',
                        'training-hosters/',
                        'all-the-facilities/'
                    ]
                }
            },
            '/de/': {
                label: 'German (Deutsch)',
                selectText: 'Sprachen',
                ariaLabel: 'Sprache auswählen',
                editLinkText: 'Diese Seite auf GitLab bearbeiten',
                lastUpdated: 'Letztes Update',
                nav: require('./nav/de'),
                sidebar: {
                    '/de/pbst/': [
                        'handbook/',
                        'ranks-and-ranking-up/',
                        'training-hosters/',
                        'all-the-facilities/'
                    ]
                }
            },
        }

    },
    sidebarDepth: 2,
    plugins: [
        [
            'vuepress-plugin-zooming',
            {
                selector: '.theme-default-content img.zooming',
                delay: 1000,
                options: {
                    bgColor: 'white',
                    zIndex: 10000,
                },
            },
        ],
        ['@vuepress/pwa',
            {
                serviceWorker: true,
                updatePopup: true
            }
        ],
    ],
})


