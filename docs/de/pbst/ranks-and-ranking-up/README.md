# Punkte und Aufstiege
## Abkürzungen
 * ST (Selbstständiges Training)
 * PBST (Pinewood Builders Security Team)

## Wie man Punkte verdient
Punkte können auf einige Weise gesammelt werden, du kannst zum Beispiel [ST](#abbreviations) machen. Sei bei einem Training dabei oder sei bei Veranstaltungen wie Raids und anderen Dingen dabei. Abhängig von dem, was du tun, erhälst du Punkte basierend auf dem, was du tust. In einem Raid kämpfen (auf der [PBST](#abbreviations) Seite).

Trainings sind auch eine Option, Trainings werden an unserer Gruppenwand angekündigt. Trainings können von folgenden Leuten veranstaltet werden:
 * Trainer
 * Tier 4

Jeder kann bei einem Training assistieren, aber der Host/Trainer bekommt die Wahl, wer Assistent sein wird. In diesem Fall hilft es nicht, den Trainer zu fragen. Dies wird deine Chancen, Assistent zu werden nur verringern, weitere Informationen über einige der Regeln finden Sie [hier](../handbook/#training).

Als letzte Option kannst du immer [ST](#abbreviations) machen. Diese Trainings wurden speziell für Leute gemacht, um Punkte auf eine andere Weise zu verdienen und um Ihre Fähigkeiten in einer bestimmten Aktivität auszuüben. Weitere Informationen erhälst du [hier](#self-trainings)

## Beförderungen

Um Befördert zu werden, brauchst du Punkte, welche du in Trainings, [ST's](#abbreviations) und Patroullien bekommen kannst. Wenn du die erforderliche Anzahl an Punkten für den nächsten Rang erhältst, wirst du befördert. Die benötigte Anzahl an Punkten für jeden Rang kann beim Spawn im [PBST Activity Center](https://www.roblox.com/games/1564828419/PBST-Activity-Center) gefunden werden. Den Teil mit den Prüfung kannst du als Tier 2 und 3 ignorieren, diese nur für Tier 1 und Tier 4~~. Das hat sich nun geändert. Bitte lese die untenstehende Nachricht...

::: danger Nicht vergessen! Wenn du `Tier 1+` bist, kannst du für Fehler, die du gemacht hast, härter bestraft werden. Du solltest ein Vorbild für alle Kadetten und Besucher in **Pinewood Anlagen** sein. Dies hat keine Ausnahme (sofern nicht anders angegeben wie die T4 Uniform Regel) 

:::

::: warning ACHTUNG Habe ein Auge auf momentane Beförderungen, es gab eine Ankündigung auf dem PBST Discord. Dies sagte, dass **Tier 1** eine Prüfung absolvieren müssen. Wie in folgender Nachricht gesagt wurde: <img class="no-medium-zoom zooming" src="https://i.imgur.com/MxDifjh.png" alt="demo" width="1228" /> 

:::

## Tier Prüfungen
Sobald du 100 Punkte erreicht hast, musst du an einer Tier Prüfung teilnehmen, wenn du den Rang **Tier  1** erhalten möchtest. Wie bei normalen Trainings finden Sie diese unter [dem Zeitplan](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). Sie werden regelmäßig veranstaltet, um alle Zeitzonen einzubringen.

Es gibt einen speziellen Server für Tier Prüfungen. Benutze den Befehl `!pbstevalserver` um dorthin zu gelangen. Dies funktioniert nur, wenn du 100+ Punkte hast.

**Eine Tier Prüfung besteht aus 2 Teilen**:
  1. Ein Quiz und ein Test auf Fähigkeiten beim Patroullieren
  2. Die Regeln für Trainings werden in diesem stark durchgesetzt und nicht befolgen, wird zu einem sofortigen Scheitern führen.

Während des Quiz erhälst du 8 Fragen zu verschiedenen Themen wie diesem Handbuch und Regeln während Trainings. Die Fragen werden sich in der Schwierigkeit unterscheiden, einige sind einfach, andere erfordern mehr Nachdenken. Du musst mindestens 5/8 Fragen richtig beantworten. Das Antworten muss privat durch einen Flüster-Chat oder durch das Privatnachrichtensystem des Veranstalters erfolgen.

Während des Patrouillentests wirst du **Tier 1** Waffen erhalten und es wird auf Fähigkeiten wie deine Fähigkeiten im Kampf, Zusammenarbeit oder die Befolgung des Handbuchs geachtet. In diesem Teil wird eine Reihe von Tier 3's gegen dich arbeiten, da sie versuchen werden, den Kern zu schmelzen oder einzufrieren.

Sobald du die Tier Prüfung bestanden hast, wirst du zu **Tier 1** befördert. Mit diesem Rang kommt eine neue Ausrüstng in Pinewood-Anlagen:  Einen stärkeren Baton, ein RIOT-Schild, einen Taser und eine Pistole. Diese Waffen finden Sie im Sicherheitsraum in Pinewood-Anlagen. Kadetten sind in den Loadout-Räumen von Tiers nicht erlaubt.

## Wann werden Punkte eingetragen?
Dies hängt davon ab, welcher Trainer verfügbar ist und die Zeit hat, Punkte einzutragen. Und wir können das nicht oft genug sagen, aber **fragen Sie Trainer nicht danach, die Punkte einzutragen**. 
Das wird nur nervig für die Trainer sein, welche die Punkte eintragen. Und es ist in der Regel Spamming wenn viele Leute **SIR SIR BITTE TRAG DIE PUNKTE EIN** sagen. 
Trainer können Punkte eintragen, das heißt sie können diese auch **WEGNEHMEN**. Als Hinweis den ich jenen geben kann, frage **NICHT** danach, ob Punkte eingetragen werden könne, es sei denn ein Trainer sagt dir, dass du sie fragen sollst. (Nehme das Bild als Beispiel)

<img class="no-medium-zoom zooming" src="https://i.imgur.com/yNHkmzm.png" alt="demo" width="1228" />

## Selbstständiges Training
Wenn du selbstständig trainierst, kannst du Punkte bekommen, ohne in einem Training zu sein, die Anzahl der Punkte ändern sich aber je nach Rang. Wenn du einen höheren Rang erhältst, erhältst du auch weniger Punkte im ST. Dies ist darauf zurückzuführen, dass du in der Lage sein solltest, ein höheres Level zu schaffen, wenn du einen höheren Rang hast.

Ein von **vgoodedward** erstelltes Bild zeigt dies auf gute Weise: <img class="no-medium-zoom zooming" src="https://i.imgur.com/ho3u0a5.png" alt="demo" width="1228" />

