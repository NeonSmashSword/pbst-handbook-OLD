---
sidebar: auto
---

# Namensnennungen
Wir würden uns gerne bei den folgenden Leuten bedanken, dass sie uns mit dem Handbuch geholfen haben:

## Backend + Entwicklung
* Stefano / superstefano4

## Übersetzungen
::: tip Übersetzungen werden gerade gemacht, da Übersetzungen manuell bestätigt werden müssen. 
Dieser Abschnitt wurde **ZEITWEISE** entfernt. Er wird zurückkehren, sobald die erste Sprache 100% Vollständigkeit erreicht. Wenn du helfen möchtest, gehe bitte zu [dieser](https://translate.pinewood-builders.ga) Webseite. Und gebe die Sprache an. (Kontaktiere mich auf [dieser E-Mail](mailto:stefano@stefanocoding.me), um deine Sprache hinzuzufügen) 
:::


## Korrekturen
### HoS
* Csd | Csdi

### PIA
* Omni | TheGreatOmni
### Trainer
* TenX
* AnuCat
* CombatSwift
* RogueVader1996
