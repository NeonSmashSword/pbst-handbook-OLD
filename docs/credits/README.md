---
sidebar: auto
---
# Credits where they're due
We would like to following people for helping us with the handbook.

## Backend + Development
* Stefano / superstefano4

## Translations
:::tip Translations are currently being made
Due to the fact that translations need to be manually approved. This section has been **TEMP** removed. It will return as soon as the first language reaches 100% completeness. 
If you would like to help, please go to [this](https://translate.pinewood-builders.ga) website. And submit a language to use. (Contact me on [this email](mailto:stefano@stefanocoding.me), to add your language)
:::


## Proofreading
### HoS
* Csd | Csdi

### PIA
* Omni | TheGreatOmni
### Trainers
* TenX 
* AnuCat
* CombatSwift
* RogueVader1996
